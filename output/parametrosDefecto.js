"use strict";

//cuando una funcion no tenga parametros, entonces adopta alguna por defecto
function registrarPersona(nombre) {
  var edad = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'no se sabe';
  var correo = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'empresa@gmail.com';
  //a correo se le asigna un valor por defecto
  return "Nombre: ".concat(nombre, ", Edad: ").concat(edad, ", Correo: ").concat(correo);
}

console.log(registrarPersona('Piero', 29, 'piero@gmail.com'));
console.log(registrarPersona('Mariella', undefined));