"use strict";

//funciones sin flecha------------------------
var usuarios = ['piero', 'pedro', 'junior'];
var num_char = usuarios.map(function (usuario) {
  console.log("El usuario ".concat(usuario, " tiene ").concat(usuario.length, " letras en su nombre")); // return `El usuario ${usuario} tiene ${usuario.length} letras en su nombre`; 
}); //funciones tipo flecha----------------
//(parametro)=>{
//  return
//}

var ciudades = ['lima', 'cusco', 'trujillo']; //const num_char2=ciudades.map((ciudad)=>`La ciudad ${ciudad} tiene ${ciudad.length} letras en su nombre`)

var num_char2 = ciudades.map(function (ciudad) {
  return "La ciudad ".concat(ciudad, " tiene ").concat(ciudad.length, " letras en su nombre");
});
console.log(num_char2); //--CONCLUSION: Las funciones tipo flecha ayudan a optimizar el codigo (reducirlo) en comparacion a las q no usan flecha