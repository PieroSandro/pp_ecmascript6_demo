"use strict";

//1RA FORMA: podemos reducir codigo como de la funcion (lineas 2 - 7) a la funcion (lineas 9 - 14) 

/*const nuevoObjeto=(nombre,edad)=>{
    return {
        nombre:nombre,
        edad:edad
    }
}

const nuevoObjeto=(nombre,edad)=>{
    return {
        nombre,
        edad
    }
}*/
//2DA FORMA: podemos incluir funcion dentro un objeto
var nuevoObjeto = function nuevoObjeto(nombre, edad) {
  return {
    nombre: nombre,
    edad: edad,
    mostrarDetalle: function mostrarDetalle() {
      return "".concat(nombre, " tiene ").concat(edad, " a\xF1os");
    }
  };
};

console.log(nuevoObjeto('alexis', 33).mostrarDetalle());