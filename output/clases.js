"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/*class Country{
    constructor(){
        
        this.name='Brasil'
        this.language='portugues'
    }
}*/
var Country = /*#__PURE__*/function () {
  function Country(name, language, population) {
    _classCallCheck(this, Country);

    //propiedades
    this.name = name, this.language = language;
    this.population = population;
  } //metodos


  _createClass(Country, [{
    key: "mostrarPoblacion",
    value: function mostrarPoblacion(cantidad) {
      return cantidad;
    }
  }, {
    key: "mostrarFicha",
    value: function mostrarFicha() {
      return "\n        <hr/>\n        Nombre oficial: ".concat(this.name, "<br/>\n        Idioma: ").concat(this.language, "<br/>\n        Poblaci\xF3n: ").concat(this.population, "<br/>\n        <hr/>\n        ");
    }
  }]);

  return Country;
}();

var peru = new Country('República del Perú', 'español', 3000000); //instancia de clase country

document.write(peru.name); //document.write(peru.mostrarPoblacion(33000));

document.write(peru.mostrarFicha()); //HERENCIA

var RegionDependiente = /*#__PURE__*/function (_Country) {
  _inherits(RegionDependiente, _Country);

  var _super = _createSuper(RegionDependiente);

  function RegionDependiente(name, language, population, dependency) {
    var _this;

    _classCallCheck(this, RegionDependiente);

    _this = _super.call(this, name, language, population); //super para el constructor de la clase padre country

    _this.dependency = dependency;
    return _this;
  }

  _createClass(RegionDependiente, [{
    key: "mostrarFicha",
    value: function mostrarFicha() {
      return "\n    <hr/>\n    Nombre oficial: ".concat(this.name, "<br/>\n    Idioma: ").concat(this.language, "<br/>\n    Poblaci\xF3n: ").concat(this.population, "<br/>\n    Dependencia: ").concat(this.dependency, "<br/>\n    <hr/>\n    ");
    }
  }]);

  return RegionDependiente;
}(Country);

var tibet = new RegionDependiente('Tibet', 'tibetano', 500000, 'China');
document.write(tibet.mostrarFicha());
/*
const suiza=new Country('Suiza','frances')
document.write(suiza.language)*/
//CONCLUSIONES:
//en la herencia, se implementan las propiedades tanto de la clase padre como la clase hijo
//en la herencia, los metodos pueden sobreescribirse