"use strict";

var mostrarDataspread = function mostrarDataspread() {
  for (var _len = arguments.length, datos = new Array(_len), _key = 0; _key < _len; _key++) {
    datos[_key] = arguments[_key];
  }

  console.log(datos);
};

var arregloDataspread = ['Franco', 30, 'piero@gmail.com', 'Peru'];
mostrarDataspread.apply(void 0, arregloDataspread); //con spread podemos enviar los elementos desde un arreglo