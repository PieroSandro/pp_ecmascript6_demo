"use strict";

var miObjeto = {
  pais: 'Peru',
  edad: 200,
  independencia: '28 julio 1921'
}; //console.log(miObjeto.independencia)

var pais = miObjeto.pais,
    edad = miObjeto.edad,
    _miObjeto$independenc = miObjeto.independencia,
    independencia = _miObjeto$independenc === void 0 ? 'No determinada' : _miObjeto$independenc;
console.log(independencia); //la funcion de la 15-17 se puede reducir en codigo, gracias al uso de la flecha

/*const mostrarData=({pais, idioma='no especificado'})=>{
 console.log(`En ${pais} se habla el idioma ${idioma}`)
}*/

var mostrarData = function mostrarData(_ref) {
  var pais = _ref.pais,
      _ref$idioma = _ref.idioma,
      idioma = _ref$idioma === void 0 ? 'no especificado' : _ref$idioma;
  return console.log("En ".concat(pais, " se habla el idioma ").concat(idioma));
};

mostrarData(miObjeto); //CONCLUSION: El procedimiento para destructurar objetos es muy similar al de destructurar arreglos