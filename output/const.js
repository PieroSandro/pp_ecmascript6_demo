//CARACTERISTICAS DE VAR, LET Y CONST
//--1-----------------------------------------------------
// con var podemos reasignar las variables las veces que queramos
// con let y const no se puede

/*var nombre="Piero S"
var nombre="luis"*/

/*const pais="mexico"
const pais="espanha"
console.log('hola '+pais)*/
//--2-----------------------------------------------------
//SEMEJANZA: usando var, let y const, no podemos acceder a las variables que estan dentro de una funcion

/*function mf1(){
    let nombre="piero"
    return 'hola '+nombre
}

console.log(nombre)*/
//--3-----------------------------------------------------
//con let y const, tienen un alcance de tipo bloque como if

/*var nota=18
if(nota>=11){
    const esAprobado=true
   
}

 console.log(esAprobado)*/
//--4----------------------------------------------------
//con const, el valor de una variable no puede cambiar en cuanto al tipo, pero sí añadirle valores en el caso que la variable const sea un arreglo
//const nombre='piero'
//nombre='hector'

/*
 const colores=['Rojo','Verde']
 colores.push('azul')
 console.log(colores)*/
//--5-----------------------------------------------------
//EN RESUMEN:
//CONST-->VARIABLES QUE NO CAMBIAN
//LET-->VARIABLES QUE SÍ CAMBIAN
//VAR--> VARIABLES QUE SÍ CAMBIAN
"use strict";