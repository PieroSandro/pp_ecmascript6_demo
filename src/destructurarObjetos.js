const miObjeto={
    pais:'Peru',
   
    edad:200,
    independencia:'28 julio 1921'
}

//console.log(miObjeto.independencia)

const {pais,edad,independencia='No determinada'}=miObjeto

console.log(independencia)

//la funcion de la 15-17 se puede reducir en codigo, gracias al uso de la flecha
/*const mostrarData=({pais, idioma='no especificado'})=>{
 console.log(`En ${pais} se habla el idioma ${idioma}`)
}*/

const mostrarData=({pais, idioma='no especificado'})=>console.log(`En ${pais} se habla el idioma ${idioma}`)


mostrarData(miObjeto)

//CONCLUSION: El procedimiento para destructurar objetos es muy similar al de destructurar arreglos
