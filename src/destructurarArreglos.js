const persona=['Piero Cóndor', 30, 'Perú']

//const nombre=persona[0]
const [nombre, ,pais,area='No determinada']=persona
console.log(nombre)
console.log(pais)

/*
const mostrarInfo=(persona)=>{
    console.log(nombre)
}

mostrarInfo(persona)*/

/*
const mostrarInfo=(nombre)=>{
    console.log(nombre)
}
mostrarInfo(nombre)*/

//EXPLICACION: en la funcion de 23-25 se pueden borrar las llaves si el codigo es corto
/*
const mostrarInfo=([nombre, ,pais]=persona)=>{
    console.log(nombre,pais)
}*/

const mostrarInfo=([nombre, ,pais,area='No se sabe']=persona)=>console.log(nombre,area)

mostrarInfo(persona)

//CONCLUSION: la destructuracion sirve para asignar un alias o un atributo descriptivo a la posicion especifica de un arreglo
