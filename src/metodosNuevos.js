//METODOS en strings
const miTexto='Hola muchachos'

console.log(miTexto,'empieza con a:', miTexto.startsWith('a'));
console.log(miTexto,'empieza con H:', miTexto.startsWith('H'));
console.log(miTexto,'empieza con h:', miTexto.toLowerCase().startsWith('h'));
console.log(miTexto,'termina con s:', miTexto.toLowerCase().endsWith('s'));
console.log(miTexto,'incluye la palabra piero:', miTexto.includes('piero'));

//METODOS en arrays
const usuarios=['piero','carlos','julio']

console.log(usuarios.includes('miguel'))

//console.log(usuarios.find((usuario)=>{ return usuario.startsWith('c')}))
console.log(usuarios.find(usuario=> usuario.startsWith('c')))

/*console.log(usuarios.findIndex((usuario)=>{
    return usuario.endsWith('s')
}))*/

console.log(usuarios.findIndex(usuario=>usuario.endsWith('s')))
