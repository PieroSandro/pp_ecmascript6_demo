//funciones sin flecha------------------------
const usuarios=['piero','pedro','junior']

const num_char=usuarios.map(function(usuario){
    console.log(`El usuario ${usuario} tiene ${usuario.length} letras en su nombre`); 
   // return `El usuario ${usuario} tiene ${usuario.length} letras en su nombre`; 
})

//funciones tipo flecha----------------
//(parametro)=>{
  //  return
//}

const ciudades=['lima','cusco','trujillo']
//const num_char2=ciudades.map((ciudad)=>`La ciudad ${ciudad} tiene ${ciudad.length} letras en su nombre`)
const num_char2=ciudades.map(ciudad=>`La ciudad ${ciudad} tiene ${ciudad.length} letras en su nombre`)
console.log(num_char2)

//--CONCLUSION: Las funciones tipo flecha ayudan a optimizar el codigo (reducirlo) en comparacion a las q no usan flecha