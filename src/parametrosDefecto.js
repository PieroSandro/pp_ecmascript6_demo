//cuando una funcion no tenga parametros, entonces adopta alguna por defecto

function registrarPersona(nombre, edad='no se sabe', correo='empresa@gmail.com'){ //a correo se le asigna un valor por defecto
    return `Nombre: ${nombre}, Edad: ${edad}, Correo: ${correo}`
}

console.log(registrarPersona('Piero',29,'piero@gmail.com'))

console.log(registrarPersona('Mariella',undefined))