//PROMESAS: es un codigo q espera algo a que suceda, y si sucede o no va ejecutar codigo

const promesa=new Promise((resolve,reject)=>{
    //resolve()//el resolve trabaja con 'then'
    //reject()//el reject trabaja con 'catch'
    setTimeout(()=>{
        const valor=1
        if(valor>3){
           // resolve('La operacion resulto exitosa')
        }else{
           // reject('La operacion NO resulto exitosa')
        }
    },3000)
})
/*
promesa.then(()=>{
    alert('hola promesa')
})

promesa.catch(()=>{
    alert('ocurrio catch')
})*/

promesa.then((mensaje)=>{
    alert(mensaje)
})

promesa.catch((mensaje)=>{
    alert(mensaje)
})