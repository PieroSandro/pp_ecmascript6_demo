//En Rest, podemos incluir un numero indefinido de parametros en una funcion

//podemos evitar de escribir muchos parametros como en la funcion (lineas 4-6)
/*const mostrarInfo=(nombre,edad,correo)=>{
    console.log(nombre,edad,correo)
}*/

//podemos recibir todos los parametros en un solo valor como 'datos' de la funcion (lineas 9-11)
const mostrarInfo=(...datos)=>{
    console.log(datos)
}

mostrarInfo('Piero',31,'piero@gmail.com','Peru')