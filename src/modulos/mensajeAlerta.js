//MANERAS DE USAR EXPORT HACIA APP.JS
//1. en modulos, usamos "export" antes de la funcion o clase para exportar
export const msgAlerta=(mensaje)=>{
   /* alert(mensaje)*/
   console.log(mensaje)
}

//2. usamos "export" aparte
const bienvenida=()=>{
    console.log('como estan')
}

export {bienvenida}