//Exportando clases
//1. una manera de exportar es usando 'export' antes del nombre de la clase: 
/*export class User{
    constructor(name,age){
        this.name=name
        this.age=age
    }

    showData(){
        console.log(`${this.name} tiene ${this.age} años`)
    }
}*/

//2. la otra forma es usando 'export' aparte de la clase:
/*class User{
    constructor(name,age){
        this.name=name
        this.age=age
    }

    showData(){
        console.log(`${this.name} tiene ${this.age} años`)
    }
}

export {User}*/

//3. otra forma es usando 'export default' cuando tengamos solo una clase:
class User{
    constructor(name,age){
        this.name=name
        this.age=age
    }

    showData(){
        console.log(`${this.name} tiene ${this.age} años`)
    }
}

export default User