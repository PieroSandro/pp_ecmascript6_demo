const buscaStr=(formVal,resultado,miArreglo)=>{
    resultado.innerHTML='';
    const texto=formVal.value.toLowerCase();
     for (let miItem of miArreglo){
         let nombre=miItem.nombre.toLowerCase();
         if(nombre.indexOf(texto)!==-1){
          resultado.innerHTML+=`<li>País: ${miItem.nombre} - Idioma: ${miItem.idioma}</li>`
         }
     }
     if(resultado.innerHTML===''){
         resultado.innerHTML +=`<li>Item no encontrado</li>`
     }
  }
 
  export {buscaStr}