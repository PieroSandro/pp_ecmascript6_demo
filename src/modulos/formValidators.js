let bNombre=false
let bCelular=false
let bEmail=false
let bMensaje=false

const checkNombre=(nombre,spNombre)=>{
    bNombre=false
    let nombreLength=nombre.value.length 
    
    if(nombreLength<3) spNombre.textContent="El nombre debe tener como mínimo 3 letras"
    else if(nombreLength>15) spNombre.textContent="El nombre debe tener como máximo 15 letras"
    else{
        spNombre.textContent=""
        bNombre=true
    }
}

const checkCelular=(celular,spCel)=>{
    bCelular=false
      let celValue=celular.value
      if(isNaN(celValue)){
          spCel.textContent="Ingrese solo números"
      }else{
          spCel.textContent=celValue.length==9?"":"La cantidad de dígitos debe ser 9"
          bCelular=spCel.textContent==""?true:false
          console.log(bCelular)
      }
}

const expRegEmail  = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const checkEmail=(email,spEmail)=>{
    bEmail=false
    let emailValue=email.value
    if(!expRegEmail.test(emailValue)){
        spEmail.textContent="No es un correo"
    }else{
        spEmail.textContent=emailValue.length<=35?"":"El correo no debe superar los 35 caracteres"
        bEmail=spEmail.textContent==""?true:false
    }
}


const checkMensaje=(mensaje,spMensaje)=>{
    bMensaje=false
    let mensajeLength=mensaje.value.length 
    
    if(mensajeLength<3) spMensaje.textContent="El mensaje debe tener como mínimo 3 caracteres"
    else if(mensajeLength>150) spMensaje.textContent="El mensaje debe tener como máximo 150 caracteres"
    else{
        spMensaje.textContent=""
        bMensaje=true
    } 
}

const submitForm=()=>{

    if(bNombre==true && bCelular==true && bEmail== true && bMensaje==true) alert('Datos correctos')
    else alert('Datos incorrectos o faltan')

   
}

export {checkNombre,checkCelular,checkEmail,checkMensaje,submitForm}