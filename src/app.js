import { msgAlerta,bienvenida} from "./modulos/mensajeAlerta";
//import {User} from './modulos/user'
import Cl from './modulos/user' //importando con uso de 'export default'

/*msgAlerta('hola a todos')

bienvenida()*/

//const renzo=new User('Renzo Julio', 34)
/*const renzo=new Cl('Renzo Julio', 34)
renzo.showData()*/


//FUNCIONES NUEVAS APLICADAS----------------------------------------------------------------
//NUEVOS IMPORTS

import {buscaStr} from "./modulos/buscadorStr"
import {checkNombre,checkCelular,checkEmail,checkMensaje,submitForm} from "./modulos/formValidators";

//1. Para buscador.html
/*const arrayPaises=[
{nombre:'Perú',idioma:'español'},
{nombre:'Brasil',idioma:'portugués'},
{nombre:'Inglaterra',idioma:'inglés'},
{nombre:'Rusia',idioma:'ruso'}
]

const inputVal=document.querySelector('#formulario')
const btnBuscar=document.querySelector('#boton')
const resultado=document.querySelector('#resultado')

btnBuscar.addEventListener('click',()=>buscaStr(inputVal,resultado,arrayPaises))
inputVal.addEventListener('keyup',()=>buscaStr(inputVal,resultado,arrayPaises))

buscaStr(inputVal,resultado,arrayPaises)*/

//2. Para formulario.html
document.querySelector("#error_nombre").style.color = "red";
document.querySelector("#error_celular").style.color = "red";
document.querySelector("#error_email").style.color = "red";
document.querySelector("#error_mensaje").style.color = "red";
//document.getElementsByClassName('sp_error').style.color="red"
const miNombre=document.querySelector('#mi_nombre')
const spanNombre=document.querySelector('#error_nombre')

miNombre.addEventListener('keyup',()=>checkNombre(miNombre,spanNombre))

const miCel=document.querySelector('#mi_celular')
const spanCel=document.querySelector('#error_celular')

miCel.addEventListener('keyup',()=>checkCelular(miCel,spanCel))

const miEmail=document.querySelector('#mi_email')
const spanEmail=document.querySelector('#error_email')

miEmail.addEventListener('keyup',()=>checkEmail(miEmail,spanEmail))

const miMensaje=document.querySelector('#mi_mensaje')
const spanMensaje=document.querySelector('#error_mensaje')

miMensaje.addEventListener('keyup',()=>checkMensaje(miMensaje,spanMensaje))

const btnEnviar=document.querySelector('#boton_enviar')

btnEnviar.addEventListener('click',submitForm)