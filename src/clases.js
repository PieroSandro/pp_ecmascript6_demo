/*class Country{
    constructor(){
        
        this.name='Brasil'
        this.language='portugues'
    }
}*/

class Country{
    constructor(name,language,population){ 
        //propiedades
        this.name=name,
        this.language=language
        this.population=population
    }

    //metodos
    mostrarPoblacion(cantidad){
        return cantidad;
    }

    mostrarFicha(){
        return `
        <hr/>
        Nombre oficial: ${this.name}<br/>
        Idioma: ${this.language}<br/>
        Población: ${this.population}<br/>
        <hr/>
        `
    }
}


const peru=new Country('República del Perú','español',3000000) //instancia de clase country
document.write(peru.name)
//document.write(peru.mostrarPoblacion(33000));
document.write(peru.mostrarFicha())


//HERENCIA
class RegionDependiente extends Country{
   constructor(name,language,population,dependency){
    super(name,language,population)   //super para el constructor de la clase padre country
    this.dependency=dependency
   }

   mostrarFicha(){
    return `
    <hr/>
    Nombre oficial: ${this.name}<br/>
    Idioma: ${this.language}<br/>
    Población: ${this.population}<br/>
    Dependencia: ${this.dependency}<br/>
    <hr/>
    `
}
}

const tibet=new RegionDependiente('Tibet','tibetano',500000,'China')
document.write(tibet.mostrarFicha())

/*
const suiza=new Country('Suiza','frances')
document.write(suiza.language)*/

//CONCLUSIONES:
//en la herencia, se implementan las propiedades tanto de la clase padre como la clase hijo
//en la herencia, los metodos pueden sobreescribirse