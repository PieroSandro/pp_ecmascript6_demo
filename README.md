PROYECTO DEMO de ecmascript 6 (ES6)

Uso de babel para compilar ES6 a js y webpack para agrupar archivos js

CONTENIDO:

Temas desarrollados:
-Template string
-Destructuración de arreglos y objetos
-Promesas
-Funcion flecha
-Uso de let y const
-Rest y spread
-Módulos
-Clases y herencia

Aplicaciones
